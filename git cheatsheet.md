# GitLab Cheat Sheet

## General Console Commands
- **Make Directory:** `mkdir "directory name"`
- **Change Directory:** `cd "directory name"`
- **Go To Parent Directory:** `cd ..`
- **List all files in the current directory:** `ls`
- **Open The Current Directory In File Explorer:** `start .`
- **Display The Contents Of The File:** `cat "file name"`
- **Create A File:** `touch "file name"`

## First Setup
- **Set Username:** `git config --global user.name "Your Name"`
- **Set Email:** `git config --global user.email "your_email@example.com"`

## Repository
- **Initialize Repository:** `git init`
- **Clone Repository:** `git clone "repository url"`
  
## Basic Commands
- **The Most Important Command There Is(Spam It):** `git status` 
- **Add To The Working Space:** `git add "file name"` (File) or `git add .` (Current Directory) or  `git add *` (All Changes)
- **Commit Everything Found On The Working Space:** `git commit -m "Commit message"`
- **Push Changes From The Current Branch To The Specified Branch :** `git push "branch name"`
- **Pull Changes From The Specified Branch To The Current Branch:** `git pull "branch name"`

## Branching
- **Create Branch:** `git branch "branch name"`
- **Switch Branch:** `git checkout "branch name"`
- **Create & Switch Branch:** `git checkout -b "branch name"`
- **Merge Branch To Current:** `git merge "branch name"`
- **Delete Branch:** `git branch -d "branch name"`
- **Put Your Current Branch On Top Of The Specified One** `git rebase "branch name"`

## Merge VS Rebase

### Git Merge
- **Action**: When you merge two branches, Git creates a new commit that includes the changes from both branches.
- **Commit History**: The commit history will show a merge commit, which can make the history look more complex and harder to follow.
- **Preservation**: Merging preserves the original commit history.
- **Use Case**: Use when you want to combine the work of two different feature branches, or when you want to integrate a completed feature branch into the main branch.

### Git Rebase
- **Action**: When you rebase a branch, Git essentially rewrites the commit history of that branch to make it look like all the changes were made in a linear fashion, starting from the latest commit in the branch you're rebasing onto.
- **Commit History**: This can make the commit history look cleaner and easier to understand, but it also means that the original commit history is lost.
- **Preservation**: Rebase does not preserve the original commit history.
- **Use Case**: Use when you want to keep a feature branch up-to-date with the latest changes from the main branch, or when you want to clean up the commit history of a branch before merging it into another branch.
  
## Viewing & Comparing
- **View Commit History:** `git log` (The Deafult Format) or `git log --pretty=oneline`(The Compact Format)
- **Show The Most Recent Commit On The Current Branch:** `git show` or  `git show "commit hash"` (Specific Commit)
- **View Changes That Haven't Been Staged Yet:** `git diff` or `git diff "commit hash"` (The Specified VS Current Commit) or `git diff "commit hash 1" "commit hash 2"` (The Specified Commits)
  
## Undoing Changes
- **Undo Uncommitted Changes:** `git checkout -- "file name"`
- **Undo Pushed Commit:** `git revert "commit hash"`

## GitLab 
- **SSH Key Generation** `ssh-keygen -t ed25519`
- **Connect Your SSh Key To Your GitLab** After generating the key use `cat "file name".pub` to read the public key, copy it and paste it on your GitLab account to connect it to your repository.
- **Verify Your Connection** `ssh -T git@gitlab.com` if every thing went fine you should see: `Welcome to GitLab, @"Your Username"!`
- **Create Merge Request:** After pushing changes to a branch, visit the GitLab repository and create a merge request.

### Remote Repository With Git
- **Establish A Connection Between With Remote Repository:** `git remote add origin "remote repository url"`
- **Push Local Master branch To The Remote Repository:** `git push origin master`
- **Pull Changes From The Remote Repository And Merge Them Into The Local Master Branch:** `git pull origin master`
- **Fetch All The Branches From The Remote Repository Without Merging Them Into Your Local Branches.** `git fetch origin`

## Miscellaneous
- **Ignore Files:** Create `.gitignore` file with files/directories to ignore when commiting
- **Stash Changes:** `git stash` (Temporarily save changes)
- **Apply Stashed Changes:** `git stash apply` (Retrieve saved changes)

## Resolve Conficts
Resolving conflicts in Git occurs during a merge or a rebase operation when the same part of the code has been modified in two different branches and Git cannot automatically reconcile the changes. Here's how to resolve conflicts:

### Step 1: Identify the Conflict
- Git will stop the merge or rebase process and alert you to the conflict.

### Step 2: Examine the Conflicted Files
- Open the conflicted file in your text editor. Git marks the areas of conflict with special markers, for example:  
<<<<<<< HEAD
Your changes to the file that are unique to the version of the file on your current branch.     
=======     
The conflicting changes from the other branch.          
\>>>>>>> branch name

**Note**: Everything I just explained here can also be done on GitLab with the help of it's UI.


### Step 3: Resolve the Conflict
- Decide which changes to keep, discard, or integrate. Edit the file to remove the conflict markers and create a unified version of the code. Some text editors like VSCode let you do all of this with the press of a button.

### Step 4: Stage the Resolved Files And Merge 
- Once resolved, stage the file  `git add "file"`and continue the commit with`git commit -m "Comment"` or `git merge --continue`.

### Step 5: Complete the Merge or Rebase
- **For Merge**: Commit the merge with `git commit -m "Resolved merge conflict in <file>"`.
- **For Rebase**: Continue the rebase with `git rebase --continue`.